---
layout: page
title: "Vancouver: Fire hydrant mapping issues"
permalink: hydrants/incorrect.html
categories: fire_hydrants
---
OpenStreetMap® is open data, licensed under the Open Data Commons Open Database License (ODbL) by the OpenStreetMap Foundation (OSMF).

## Duplicate fire hydrant codes
